# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2019-01-18 14:46+0100\n"
"PO-Revision-Date: 2018-10-26 13:20+0000\n"
"Last-Translator: Weblate Admin <admin@example.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Weblate 2.19.1\n"

#. type: Title -
#, no-wrap
msgid "Test your Wi-Fi\n"
msgstr ""

#. type: Plain text
msgid ""
"Problems with Wi-Fi are unfortunately quite common in Tails and Linux in "
"general. To test if your Wi-Fi interface works in Tails:"
msgstr ""

#. type: Bullet: '1. '
msgid "Open the system menu in the top-right corner:"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "   [[!img doc/first_steps/introduction_to_gnome_and_the_tails_desktop/system.png link=\"no\"]]\n"
msgstr ""

#. type: Bullet: '1. '
msgid ""
"Choose <span class=\"guilabel\">Wi-Fi Not Connected</span> and then <span "
"class=\"guilabel\">Select Network</span>."
msgstr ""

#. type: Bullet: '1. '
msgid "After establishing a connection to a network:"
msgstr ""

#. type: Bullet: '   - '
msgid "If you can already access the Internet, Tor is automatically started."
msgstr ""

#. type: Bullet: '   - '
msgid ""
"If you need to log in to a captive portal before being granted access to the "
"Internet, see our documentation about [[logging in to captive portals|doc/"
"anonymous_internet/unsafe_browser]]."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"doc/anonymous_internet/networkmanager/no-wifi.inline\" raw=\"yes\" sort=\"age\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<div class=\"trophy\">\n"
msgstr "<div class=\"trophy\">\n"

#. type: Plain text
#, no-wrap
msgid "<p>Yay, you managed to start <span class=\"install-clone mac-clone\">your new</span> Tails on your computer!</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<p>If you want to save some of your documents\n"
"and configuration in an encrypted storage on your <span class=\"clone\">new</span> Tails USB stick, follow\n"
"our instructions until the end. Otherwise, have a look at our\n"
"<span class=\"install-clone\">[[final recommendations|clone#recommendations]].</span>\n"
"<span class=\"expert\">[[final recommendations|expert/usb#recommendations]].</span>\n"
"<span class=\"windows\">[[final recommendations|win/usb#recommendations]].</span>\n"
"<span class=\"mac\">[[final recommendations|mac/usb#recommendations]].</span>\n"
"<span class=\"mac-clone\">[[final recommendations|mac/clone#recommendations]].</span>\n"
"<span class=\"linux\">[[final recommendations|linux/usb#recommendations]].</span>\n"
"</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "</div>\n"
msgstr "</div>\n"

#. type: Plain text
#, no-wrap
msgid "<h1 id=\"create-persistence\">Create an encrypted persistent storage (optional)</h1>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<div class=\"step-image\">[[!img install/inc/infography/create-persistence.png link=\"no\" alt=\"\"]]</div>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"You can optionally create an *encrypted persistent storage* in the\n"
"remaining free space on your\n"
"<span class=\"clone\">new</span>\n"
"Tails USB stick to store any of the following:\n"
msgstr ""

#. type: Bullet: '  - '
msgid "Personal files"
msgstr ""

#. type: Bullet: '  - '
msgid "Some settings"
msgstr ""

#. type: Bullet: '  - '
msgid "Additional software"
msgstr ""

#. type: Bullet: '  - '
msgid "Encryption keys"
msgstr ""

#. type: Plain text
msgid "The data in the encrypted persistent storage:"
msgstr ""

#. type: Bullet: '  - '
msgid "Remains available across separate working sessions."
msgstr ""

#. type: Bullet: '  - '
msgid "Is encrypted using a passphrase of your choice."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<div class=\"caution\">\n"
msgstr "<div class=\"caution\">\n"

#. type: Plain text
#, no-wrap
msgid ""
"<p>The encrypted persistent storage is not hidden. An attacker in possession of\n"
"the USB stick can know whether it has an encrypted persistent storage. Take into consideration\n"
"that you can be forced or tricked to give out its passphrase.</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<p>It is possible to\n"
"open the encrypted persistent storage from other operating systems. But, doing\n"
"so might compromise the security provided by Tails.</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<p>For example, image thumbnails might be created and saved by the other operating\n"
"system. Or, the contents of files might be indexed by the other operating\n"
"system.</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<p>Other operating systems should probably not be trusted to handle\n"
"sensitive information or leave no trace.</p>\n"
msgstr ""

#. type: Title -
#, no-wrap
msgid "Create the persistent storage\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"1. Choose\n"
"   <span class=\"menuchoice\">\n"
"     <span class=\"guimenu\">Applications</span>&nbsp;▸\n"
"     <span class=\"guisubmenu\">Tails</span>&nbsp;▸\n"
"     <span class=\"guimenuitem\">Configure persistent volume</span></span>.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"1. Specify a passphrase of your choice in both the\n"
"<span class=\"guilabel\">Passphrase</span> and <span class=\"guilabel\">Verify\n"
"Passphrase</span> text boxes.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"   <div class=\"tip\">\n"
"   <p>We recommend choosing a long passphrase made of five to seven random words.\n"
"   <a href=\"https://theintercept.com/2015/03/26/passphrases-can-memorize-attackers-cant-guess/\">Learn more.</a></p>\n"
"   </div>\n"
msgstr ""

#. type: Bullet: '1. '
msgid "Click on the <span class=\"guilabel\">Create</span> button."
msgstr ""

#. type: Bullet: '2. '
msgid "Wait for the creation to finish."
msgstr ""

#. type: Bullet: '3. '
msgid ""
"The assistant shows a list of the possible persistence features. Each "
"feature corresponds to a set of files or settings to be saved in the "
"encrypted persistent storage."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"   We recommend you to only activate the <span class=\"guilabel\">Personal\n"
"   Data</span> persistence feature for the time being. You\n"
"   can activate more features later on according to your needs.\n"
msgstr ""

#. type: Bullet: '4. '
msgid "Click <span class=\"button\">Save</span>."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "   <div class=\"step-image\">[[!img install/inc/infography/restart-on-tails.png link=\"no\" alt=\"\"]]</div>\n"
msgstr ""

#. type: Title -
#, no-wrap
msgid "Restart and activate the persistent storage\n"
msgstr ""

#. type: Bullet: '1. '
msgid ""
"Shut down the computer and restart on your <span class=\"clone\">new</span> "
"Tails USB stick."
msgstr ""

#. type: Bullet: '1. '
msgid "In <span class=\"application\">Tails Greeter</span>:"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "   [[!img install/inc/screenshots/greeter_with_persistence.png link=\"no\" alt=\"Tails Greeter: 'Welcome to Tails!'\"]]\n"
msgstr ""

#. type: Bullet: '   - '
msgid ""
"Select your language and keyboard layout in the <span class=\"guilabel"
"\">Language & Region</span> section."
msgstr ""

#. type: Bullet: '   - '
msgid ""
"In the <span class=\"guilabel\">Encrypted Persistent Storage</span> section, "
"enter your passphrase and click <span class=\"button\">Unlock</span> to "
"activate the encrypted persistent storage for the current working session."
msgstr ""

#. type: Bullet: '   - '
msgid "Click <span class=\"button\">Start Tails</span>."
msgstr ""

#. type: Bullet: '1. '
msgid "After 15&ndash;30 seconds, the Tails desktop appears."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"1. You can now save your personal files and working documents in the\n"
"<span class=\"guilabel\">Persistent</span> folder. To open the\n"
"<span class=\"guilabel\">Persistent</span> folder choose\n"
"<span class=\"menuchoice\">\n"
"  <span class=\"guimenu\">Places</span>&nbsp;▸\n"
"  <span class=\"guimenuitem\">Persistent</span></span>.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<div class=\"trophy\" id=\"recommendations\">\n"
msgstr "<div class=\"trophy\" id=\"recommendations\">\n"

#. type: Plain text
#, no-wrap
msgid "<div class=\"state-image\">[[!img install/inc/infography/tails-usb-with-persistence.png link=\"no\" alt=\"Tails USB stick with persistent storage\"]]</div>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<p>You now have a complete Tails, congrats!</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<h3>Final recommendations</h3>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<div class=\"row\">\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "  <div class=\"col-md-6\">\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "    [[!img lib/dialog-warning.png link=\"no\" alt=\"\" class=\"float-left\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"    <p>Tails does not protect you from everything! Have a look at our\n"
"    [[warnings|doc/about/warning]].</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "  </div>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "    [[!img lib/help-browser.png link=\"no\" alt=\"\" class=\"float-left\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"    <p>If you face any problem, use the <span class=\"guilabel\">Report an\n"
"    error</span> launcher on the Tails desktop or visit our [[support\n"
"    pages|support]].</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<p>We hope you enjoy using Tails :)</p>\n"
msgstr ""
