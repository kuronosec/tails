# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2019-09-24 21:17+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Content of: <h2>
msgid "Make the computer start on the USB stick"
msgstr ""

#. type: Content of: <p>
msgid ""
"This step is the most complicated one as it depends a lot on the model of "
"the computer:"
msgstr ""

#. type: Content of: <ul><li>
msgid ""
"On most computers, our basic instructions to get to the boot menu work fine."
msgstr ""

#. type: Content of: <ul><li>
msgid ""
"On some computers, you might have to do more advanced techniques, like "
"editing the BIOS settings."
msgstr ""

#. type: Content of: <ul><li>
msgid ""
"On a few computers, it might currently be impossible to start Tails.  But, "
"it is very hard to know without trying."
msgstr ""

#. type: Content of: <p>
msgid ""
"The goal is to reach the <span class=\"application\">Boot Loader Menu</span> "
"of Tails:"
msgstr ""

#. type: Content of: <p>
msgid ""
"[[!img install/inc/screenshots/boot_loader_menu.png link=\"no\" alt=\"Black "
"screen with Tails artwork. Boot Loader Menu with two options 'Tails' and "
"'Tails (Troubleshooting Mode)'.\"]]"
msgstr ""

#. type: Content of: <div><p>
msgid ""
"After you reach the <span class=\"application\">Boot Loader Menu</span>, you "
"can skip the rest of this section and <a href=\"#greeter\">wait until <span "
"class=\"application\">Tails Greeter</span> appears</a>."
msgstr ""

#. type: Content of: <h3>
msgid "Getting to the boot menu"
msgstr ""

#. type: Content of: <p>
msgid ""
"On most computers, you can press a <em>boot menu key</em> to display a list "
"of possible devices to start from. The following instructions explain how to "
"display the boot menu and start on the USB stick. The following screenshot "
"is an example of a boot menu:"
msgstr ""

#. type: Content of: <p>
msgid ""
"[[!img install/inc/screenshots/bios_boot_menu.png link=\"no\" alt=\"\"]]"
msgstr ""

#. type: Content of: <ol><li><p>
msgid "Shut down the computer while leaving the USB stick plugged in."
msgstr ""

#. type: Content of: <ol><li><p>
msgid "Shut down the computer."
msgstr ""

#. type: Content of: <ol><li><p>
msgid ""
"Plug in the other Tails USB stick that you want to <span class=\"install-"
"clone\">install</span> <span class=\"upgrade\">upgrade</span> from."
msgstr ""

#. type: Content of: <ol><li><p>
msgid ""
"Unplug your Tails USB stick while leaving the intermediary USB stick plugged "
"in."
msgstr ""

#. type: Content of: <ol><li><p>
msgid ""
"Identify the possible boot menu keys for the computer depending on the "
"computer manufacturer in the following list:"
msgstr ""

#. type: Content of: <ol><li><table><tr><th>
msgid "Manufacturer"
msgstr ""

#. type: Content of: <ol><li><table><tr><th>
msgid "Key"
msgstr ""

#. type: Content of: <ol><li><table><tr><td>
msgid "Acer"
msgstr ""

#. type: Content of: <ol><li><table><tr><td>
msgid "Esc, F12, F9"
msgstr ""

#. type: Content of: <ol><li><table><tr><td>
msgid "Asus"
msgstr ""

#. type: Content of: <ol><li><table><tr><td>
msgid "Esc, F8"
msgstr ""

#. type: Content of: <ol><li><table><tr><td>
msgid "Dell"
msgstr ""

#. type: Content of: <ol><li><table><tr><td>
msgid "F12"
msgstr ""

#. type: Content of: <ol><li><table><tr><td>
msgid "Fujitsu"
msgstr ""

#. type: Content of: <ol><li><table><tr><td>
msgid "F12, Esc"
msgstr ""

#. type: Content of: <ol><li><table><tr><td>
msgid "HP"
msgstr ""

#. type: Content of: <ol><li><table><tr><td>
msgid "F9, Esc"
msgstr ""

#. type: Content of: <ol><li><table><tr><td>
msgid "Lenovo"
msgstr ""

#. type: Content of: <ol><li><table><tr><td>
msgid "F12, Novo, F8, F10"
msgstr ""

#. type: Content of: <ol><li><table><tr><td>
msgid "Samsung"
msgstr ""

#. type: Content of: <ol><li><table><tr><td>
msgid "Esc, F12, F2"
msgstr ""

#. type: Content of: <ol><li><table><tr><td>
msgid "Sony"
msgstr ""

#. type: Content of: <ol><li><table><tr><td>
msgid "F11, Esc, F10"
msgstr ""

#. type: Content of: <ol><li><table><tr><td>
msgid "Toshiba"
msgstr ""

#. type: Content of: <ol><li><table><tr><td>
msgid "others&hellip;"
msgstr ""

#. type: Content of: <ol><li><div><p>
msgid ""
"On many computers, a message is displayed very briefly when switching on "
"that also explains how to get to the boot menu or edit the BIOS settings."
msgstr ""

#. type: Content of: <ol><li><p>
msgid ""
"Switch on the computer and immediately press several times the first "
"possible boot menu key identified in step 2."
msgstr ""

#. type: Content of: <ol><li><p>
msgid ""
"If the computer starts on another operating system or returns an error "
"message, shut down the computer again and repeat step 3 for all the possible "
"boot menu keys identified in step 2."
msgstr ""

#. type: Content of: <ol><li><p>
msgid ""
"If a boot menu with a list of devices appears, select your USB stick and "
"press <span class=\"keycap\">Enter</span>."
msgstr ""

#. type: Content of: <ol><li><div><p>
msgid ""
"If the boot menu key that works on your computer is a different one than the "
"first in the list, please let us know. You can write to [[sajolida@pimienta."
"org]] (private email)."
msgstr ""

#. type: Content of: <p>
msgid "This animation explains how to use the boot menu key:"
msgstr ""

#. type: Content of: <h3>
msgid "Check our list of known issues"
msgstr ""

#. type: Content of: <p>
msgid ""
"Similar problems might have been reported already for your model of computer "
"in our [[list of known issues|support/known_issues]]."
msgstr ""

#. type: Content of: <h3>
msgid "Edit the BIOS settings"
msgstr ""

#. type: Content of: <p>
msgid ""
"If none of the possible boot menu keys from the previous technique work, or "
"if the USB stick does not appear in the boot menu, you might need to edit "
"your BIOS settings."
msgstr ""

#. type: Content of: <div><p>
msgid ""
"Search for the user manual of the computer on the website of its "
"manufacturer to learn how to edit the BIOS settings:"
msgstr ""

#. type: Content of: <div><ul><li>
msgid "<a href=\"http://us.acer.com/ac/en/US/content/drivers\">Acer</a>"
msgstr ""

#. type: Content of: <div><ul><li>
msgid "<a href=\"https://www.asus.com/us/support/\">Asus</a>"
msgstr ""

#. type: Content of: <div><ul><li>
msgid ""
"<a href=\"https://www.dell.com/support/home/us/en/19/Products/\">Dell</a>"
msgstr ""

#. type: Content of: <div><ul><li>
msgid ""
"<a href=\"https://www.fujitsu.com/global/support/products/software/manual/"
"\">Fujitsu</a>"
msgstr ""

#. type: Content of: <div><ul><li>
msgid "<a href=\"https://support.hp.com/us-en/products/\">HP</a>"
msgstr ""

#. type: Content of: <div><ul><li>
msgid "<a href=\"http://support.lenovo.com/us/en/\">Lenovo</a>"
msgstr ""

#. type: Content of: <div><ul><li>
msgid "<a href=\"http://www.samsung.com/us/support/downloads\">Samsung</a>"
msgstr ""

#. type: Content of: <div><ul><li>
msgid "<a href=\"http://esupport.sony.com/US\">Sony</a>"
msgstr ""

#. type: Content of: <div><ul><li>
msgid ""
"<a href=\"http://support.toshiba.com/support/products?cat=Computers"
"\">Toshiba</a>"
msgstr ""

#. type: Content of: <p>
msgid ""
"In the BIOS settings, try to apply the following changes one by one and "
"restart the computer after each change. Some changes might not apply to "
"certain computer models."
msgstr ""

#. type: Content of: <div><p>
msgid ""
"Take note of the changes that you apply to the BIOS settings. That way, you "
"can revert the changes if they prevent the computer from starting on its "
"usual operating system."
msgstr ""

#. type: Content of: <ol><li><p>
msgid ""
"Edit the <strong>Boot Order</strong>. Depending on the computer model you "
"might see an entry for <strong>removable devices</strong> or <strong>USB "
"media</strong>. Move this entry to the top of the list to force the computer "
"to try to start from the USB stick before starting from the internal hard "
"disk."
msgstr ""

#. type: Content of: <ol><li><p>
msgid "Disable <strong>Fast boot</strong>."
msgstr ""

#. type: Content of: <ol><li><p>
msgid ""
"If the computer is configured to start with <strong>legacy BIOS</strong>, "
"try to configure it to start with <strong>UEFI</strong>. Else, if the "
"computer is configured to start with <strong>UEFI</strong>, try to configure "
"it to start with <strong>legacy BIOS</strong>. To do so, try any of the "
"following options if available:"
msgstr ""

#. type: Content of: <ol><li><ul><li>
msgid "Enable <strong>Legacy mode</strong>"
msgstr ""

#. type: Content of: <ol><li><ul><li>
msgid ""
"Disable <strong>Secure boot</strong> (see the <a href=\"https://docs."
"microsoft.com/en-us/windows-hardware/manufacture/desktop/disabling-secure-"
"boot\">Microsoft documentation</a>)"
msgstr ""

#. type: Content of: <ol><li><ul><li>
msgid "Enable <strong>CSM boot</strong>"
msgstr ""

#. type: Content of: <ol><li><ul><li>
msgid "Disable <strong>UEFI</strong>"
msgstr ""

#. type: Content of: <ol><li><p>
msgid ""
"Try to upgrade the BIOS to the latest version provided by the manufacturer."
msgstr ""

#. type: Content of: <div><p>
msgid ""
"If none of these options work, we are sorry but you might not be able to use "
"Tails on this computer. Feel free to [[!toggle id=\"report-toggle-2\" text="
"\"report the problem to our help desk\"]]."
msgstr ""

#. type: Content of: outside any tag (error?)
msgid ""
"[[!toggleable id=\"report-toggle-2\" text=\"\"\" <span class=\"hide\">[[!"
"toggle id=\"report-toggle-2\" text=\"\"]]</span> [[!inline pages=\"install/"
"inc/steps/reporting.inline\" raw=\"yes\"]] \"\"\"]]"
msgstr ""
