# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2018-07-28 08:15+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Enable & use the persistent volume\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"[[!inline pages=\"doc/first_steps/persistence.caution\" raw=\"yes\" "
"sort=\"age\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!toc levels=1]]\n"
msgstr ""

#. type: Title =
#, no-wrap
msgid "Enable the persistent volume\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"When starting Tails, in the\n"
"<span class=\"guilabel\">Encrypted Persistent Storage</span> section of\n"
"[[<span class=\"application\">Tails "
"Greeter</span>|startup_options#tails_greeter]], enter your passphrase and "
"click\n"
"<span class=\"button\">Unlock</span>.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!img startup_options/persistence.png link=\"no\" alt=\"\"]]\n"
msgstr ""

#. type: Title =
#, no-wrap
msgid "Use the persistent volume\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"To open the <span class=\"filename\">Persistent</span> folder and access "
"your\n"
"personal files and working documents, choose \n"
"<span class=\"menuchoice\">\n"
"  <span class=\"guimenu\">Places</span>&nbsp;▸\n"
"  <span class=\"guimenuitem\">Persistent</span></span>.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"For advanced users, to access the internal content of the persistent "
"volume\n"
"choose\n"
"<span class=\"menuchoice\">\n"
"  <span class=\"guimenu\">Places</span>&nbsp;▸\n"
"  <span class=\"guimenuitem\">Computer</span></span>, and open the folders\n"
"  <span class=\"filename\">live</span>&nbsp;▸\n"
"  <span class=\"filename\">persistence</span>&nbsp;▸\n"
"  <span class=\"filename\">TailsData_unlocked</span>.\n"
msgstr ""
