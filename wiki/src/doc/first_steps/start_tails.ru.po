# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2018-11-07 15:43+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Start Tails\"]]\n"
msgstr ""

#. type: Plain text
msgid "To learn how to start Tails, refer to our installation instructions on:"
msgstr ""

#. type: Bullet: '  - '
msgid "[[Starting a PC on a USB|install/win/usb#start-tails]]"
msgstr ""

#. type: Bullet: '  - '
msgid "[[Starting a Mac on a USB|install/mac/usb#start-tails]]"
msgstr ""
