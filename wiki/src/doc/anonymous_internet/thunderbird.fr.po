# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Tails\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2019-05-21 17:19+0000\n"
"PO-Revision-Date: 2019-09-23 14:54+0000\n"
"Last-Translator: xin <xin@riseup.net>\n"
"Language-Team: Tails translators <tails@boum.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 2.20\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Emailing and reading news with Thunderbird\"]]\n"
msgstr "[[!meta title=\"Envoyer des courriers électroniques et lire des flux avec Thunderbird\"]]\n"

#. type: Plain text
#, no-wrap
msgid "Tails includes <span class=\"application\">Thunderbird</span> for:\n"
msgstr "Tails inclut <span class=\"application\">Thunderbird</span> pour :\n"

#. type: Plain text
msgid ""
"- Reading and writing emails - Reading [[!wikipedia RSS]] and [[!wikipedia "
"Atom_(Web_standard) desc=\"Atom\"]] feeds for news and blogs"
msgstr ""
"- Lire et écrire des courriers électroniques - Lire des flux [[!wikipedia_fr "
"RSS]] et [[!wikipedia_fr Atom_Syndication_Format desc=\"Atom\"]] de sites "
"d'actualité et de blogs"

#. type: Plain text
#, no-wrap
msgid "[[!toc levels=2]]\n"
msgstr "[[!toc levels=2]]\n"

#. type: Plain text
#, no-wrap
msgid ""
"To start <span class=\"application\">Thunderbird</span> choose\n"
"<span class=\"menuchoice\">\n"
"  <span class=\"guimenu\">Applications</span>&nbsp;▸\n"
"  <span class=\"guisubmenu\">Internet</span>&nbsp;▸\n"
"  <span class=\"guimenuitem\">Thunderbird</span></span>.\n"
msgstr ""
"Pour démarrer <span class=\"application\">Thunderbird</span> choisissez\n"
"<span class=\"menuchoice\">\n"
"  <span class=\"guimenu\">Applications</span>&nbsp;▸\n"
"  <span class=\"guisubmenu\">Internet</span>&nbsp;▸\n"
"  <span class=\"guimenuitem\">Messagerie Thunderbird</span></span>.\n"

#. type: Plain text
#, no-wrap
msgid "<div class=\"tip\">\n"
msgstr "<div class=\"tip\">\n"

#. type: Plain text
#, no-wrap
msgid ""
"<p>You can activate the [[<strong>Thunderbird</strong> persistence\n"
"feature|doc/first_steps/persistence/configure#thunderbird]] to store your\n"
"emails, feeds, and settings across separate working sessions.</p>\n"
msgstr ""
"<p>Vous pouvez activer l'[[option de persistance <strong>Thunderbird</strong>\n"
"|doc/first_steps/persistence/configure#thunderbird]] pour stocker vos\n"
"courriers, flux et réglages entre plusieurs sessions de travail distinctes.</p>\n"

#. type: Plain text
#, no-wrap
msgid "</div>\n"
msgstr "</div>\n"

#. type: Plain text
#, no-wrap
msgid ""
"For more detailed documentation, refer to the [official\n"
"<span class=\"application\">Thunderbird</span>\n"
"help](https://support.mozilla.org/en-US/products/thunderbird).\n"
msgstr ""
"Pour une documentation détaillée, consultez [l'aide\n"
"<span class=\"application\">Thunderbird</span>\n"
"officielle](https://support.mozilla.org/fr/products/thunderbird).\n"

#. type: Title =
#, no-wrap
msgid "Configure an email account\n"
msgstr "Configurer un compte de messagerie électronique\n"

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"doc/anonymous_internet/thunderbird/account_creation.inline\" raw=\"yes\" sort=\"age\"]]\n"
msgstr "[[!inline pages=\"doc/anonymous_internet/thunderbird/account_creation.inline.fr\" raw=\"yes\" sort=\"age\"]]\n"

#. type: Title =
#, no-wrap
msgid "OpenPGP encryption with Enigmail\n"
msgstr "Chiffrement OpenPGP avec Enigmail\n"

#. type: Plain text
#, no-wrap
msgid ""
"<span class=\"application\">Thunderbird</span> in Tails includes the\n"
"<span class=\"application\">[Enigmail](https://www.enigmail.net/)</span>\n"
"extension to encrypt and authenticate emails using OpenPGP.\n"
msgstr ""
"<span class=\"application\">Thunderbird</span> dans Tails inclut\n"
"<span class=\"application\">[Enigmail](https://www.enigmail.net/)</span>\n"
"une extension pour chiffrer et authentifier les messages électroniques en utilisant OpenPGP.\n"

#. type: Plain text
#, no-wrap
msgid ""
"To configure <span class=\"application\">Enigmail</span> for your email account, you can start the\n"
"<span class=\"application\">Enigmail Setup Wizard</span> by choosing\n"
"<span class=\"menuchoice\">\n"
"  <span class=\"guimenu\">[[!img lib/open-menu.png alt=\"Menu\" class=symbolic link=no]]</span>&nbsp;▸\n"
"  <span class=\"guisubmenu\">Enigmail</span>&nbsp;▸\n"
"  <span class=\"guimenuitem\">Setup Wizard</span></span>.\n"
msgstr ""
"Pour configurer <span class=\"application\">Enigmail</span> pour votre compte de messagerie, vous pouvez démarrer\n"
"l'<span class=\"application\">Assistant de configuration Enigmail</span> en choisissant\n"
"<span class=\"menuchoice\">\n"
"  <span class=\"guimenu\">[[!img lib/open-menu.png alt=\"Menu\" class=symbolic link=no]]</span>&nbsp;▸\n"
"  <span class=\"guisubmenu\">Enigmail</span>&nbsp;▸\n"
"  <span class=\"guimenuitem\">Assistant de configuration</span></span>.\n"

#. type: Plain text
#, no-wrap
msgid ""
"You can also create an OpenPGP key associated with your email address as\n"
"part of the <span class=\"application\">Enigmail Setup Wizard</span>.\n"
msgstr ""
"Vous pouvez aussi créer une clé OpenPGP associée avec votre adresse de messagerie électronique\n"
"avec l'utilitaire <span class=\"application\">Assistant de configuration Enigmail</span>.\n"

#. type: Plain text
#, no-wrap
msgid ""
"If you are new to OpenPGP, you can read the following guides on getting\n"
"started with encrypting emails using\n"
"<span class=\"application\">Thunderbird</span> and\n"
"<span class=\"application\">Enigmail</span>:\n"
msgstr ""
"Si vous êtes débutant avec OpenPGP, vous pouvez lire les manuels suivants\n"
"sur comment débuter avec les courriers électroniques chiffrés en utilisant\n"
"<span class=\"application\">Thunderbird</span> et\n"
"<span class=\"application\">Enigmail</span> :\n"

#. type: Bullet: '  - '
msgid ""
"[Security-in-a-Box: Thunderbird & OpenPGP - secure email](https://"
"securityinabox.org/en/guide/thunderbird/linux/)"
msgstr ""
"[Security-in-a-Box : Thunderbird et OpenPGP - courriel sécurisé](https://"
"securityinabox.org/fr/guide/thunderbird/linux/)"

#. type: Bullet: '  - '
msgid ""
"[Enigmail: Enigmail Quick Start Guide](https://www.enigmail.net/index.php/en/"
"user-manual/quick-start)"
msgstr ""
"[Enigmail: Enigmail Quick Start Guide](https://www.enigmail.net/index.php/en/"
"user-manual/quick-start) (en anglais)"

#. type: Title =
#, no-wrap
msgid "Enhanced privacy with TorBirdy\n"
msgstr "Amélioration de la confidentialité avec TorBirdy\n"

#. type: Plain text
#, no-wrap
msgid ""
"<span class=\"application\">Thunderbird</span> in Tails includes the\n"
"<span class=\"application\">[TorBirdy](https://trac.torproject.org/projects/tor/wiki/torbirdy)</span>\n"
"extension for additional privacy and anonymity.\n"
msgstr ""
"<span class=\"application\">Thunderbird</span> dans Tails inclut\n"
"<span class=\"application\">[TorBirdy](https://trac.torproject.org/projects/tor/wiki/torbirdy)</span>\n"
"une extension pour plus de confidentialité et d'anonymat.\n"

#. type: Plain text
#, no-wrap
msgid "<div class=\"note\">\n"
msgstr "<div class=\"note\">\n"

#. type: Plain text
#, no-wrap
msgid ""
"<p>For more security,\n"
"<span class=\"application\">TorBirdy</span> disables some features of\n"
"<span class=\"application\">Thunderbird</span>:\n"
msgstr ""
"<p>Pour plus de sécurité,\n"
"<span class=\"application\">TorBirdy</span> désactive certaines fonctionnalités de\n"
"<span class=\"application\">Thunderbird</span> :\n"

#. type: Plain text
#, no-wrap
msgid ""
"<ul>\n"
"<li>Sending emails and displaying feeds in HTML format.<br/>Emails and feeds in HTML format are displayed in plain text and can become harder to read.</li>\n"
"<li>Automatic checking of Atom and RSS feeds on startup.</li>\n"
"</ul>\n"
msgstr ""
"<ul>\n"
"<li>L'envoi de courrier électronique et l'affichage de flux au format HTML.<"
"br/>Les courriers électroniques et les flux au format HTML sont affichés en "
"texte brut et peuvent devenir plus difficiles à lire.</li>\n"
"<li>La vérification automatique au démarrage des flux Atom et RSS.</li>\n"
"</ul>\n"

#. type: Plain text
#, no-wrap
msgid ""
"To learn more about the security properties of <span class=\"application\">TorBirdy</span>,\n"
"you can read its [design document](https://trac.torproject.org/projects/tor/attachment/wiki/doc/TorifyHOWTO/EMail/Thunderbird/Thunderbird%2BTor.pdf).\n"
msgstr ""
"Pour en savoir plus sur les caractéristiques de sécurité de <span class=\"application\">TorBirdy</span>,\n"
"vous pouvez lire sa [documentation de conception](https://trac.torproject.org/projects/tor/attachment/wiki/doc/TorifyHOWTO/EMail/Thunderbird/Thunderbird%2BTor.pdf) (en anglais).\n"

#. type: Title =
#, no-wrap
msgid "Using Thunderbird in your language\n"
msgstr "Utiliser Thunderbird dans votre langue\n"

#. type: Plain text
#, no-wrap
msgid ""
"To use <span class=\"application\">Thunderbird</span> in your language, you can\n"
"install the <span class=\"command\">thunderbird-l10n-<span class=\"command-placeholder\">lang</span></span>\n"
"package using the [[Additional Software|doc/first_steps/additional_software]]\n"
"feature. Replace <span class=\"command-placeholder\">lang</span> with the code\n"
"for your language. For example, <span class=\"command\">es</span> for Spanish or\n"
"<span class=\"command\">de</span> for German.\n"
msgstr ""
"Pour utiliser <span class=\"application\">Thunderbird</span> dans votre "
"langue, vous pouvez\n"
"installer le paquet <span class=\"command\">thunderbird-l10n-<span class"
"=\"command-placeholder\">lang</span></span>\n"
"en utilisant la fonctionnalité [[Logiciels additionnels|doc/first_steps/"
"additional_software]].\n"
"Remplacez <span class=\"command-placeholder\">lang</span> par le code\n"
"de votre langue. Par exemple, <span class=\"command\">es</span> pour "
"l'espagnol ou\n"
"<span class=\"command\">de</span> pour l'allemand.\n"
