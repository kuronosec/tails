[[!meta title="Known issues with graphics cards"]]

- For other hardware compatibility issues, refer to our [[known issues|support/known_issues]].

[[!toc levels=2]]

Error starting GDM
==================

This section applies if you see the following error message when
starting Tails:

<p class="pre">
Error starting GDM with your graphics card: <i>name of your graphics
card [id] (rev number)</i>. Please take note of this error and visit
https://tails.boum.org/gdm for troubleshooting.
</p>

1. Identify the name, ID, and revision number (if any) of your graphics card.

   For example, if your error message starts with:

   <p class="pre">Error starting GDM with your graphics card:
   NVIDIA Corporation GT218M [NVS 3100M] [10de:0a6c] (rev a2)</p>

   - The name is *NVIDIA Corporation GT218M [NVS 3100M]*.
   - The ID is *[10de:0a6c]*. The ID is unique to the model of your
     graphics card, it is not unique to your computer.
   - The revision number is *a2*. Your graphics card might have no
     revision number.

1. Check if your graphics card is listed below. For example, you can search for its
   name or ID on this page.

   - If your graphics card is listed, check if a workaround is
     documented to make it work on Tails.

     If the workaround doesn't work, please [[report the problem to our
     help desk|support/talk]].

     Mention in your email:

     - The version of Tails that you are trying to start.
     - The name, ID, and revision number (if any) of your graphics card.
     - The workaround that you tried and that failed.

   - If your graphics card is not listed, please [[contact our support
     team by email|support/talk]].

     Mention in your email:

     - The version of Tails that you are trying to start.
     - The name, ID, and revision number (if any) of your graphics card.

     <div class="tip">

     <p>You can send us a photo of the error message as it appears on
     your screen.</p>

     </div>

1. If your problem get fixed in a future version of Tails, please let us
   know so we can update this page.

<!--

Name and ID in /usr/share/misc/pci.ids
======================================

The correspondence between the name and ID is established in
/usr/share/misc/pci.ids.

For example:

	8086  Intel Corporation
	        0007  82379AB
	        [...]
	        0046  Core Processor Integrated Graphics Controller

Corresponds to:

	Intel Corporation Core Processor Integrated Graphics Controller [8086:0046]

Template for new section
========================

<a id="$ANCHOR"></a>

$FAMILY_NAME
------------

$LT!--
Tickets: #XXXXX #XXXXX
--$GT

### Affected graphics cards

<table>
<tr><th>Name</th><th>ID</th><th>Revision number</th></tr>
<tr><td>$VENDOR $DEVICE</td><td>[$VENDOR_ID:$PRODUCT_ID]</td><td>(rev $REVISION_NUMBER)</td></tr>
</table>

### Workaround

$WORKAROUND_IF_ANY

-->

<a id="radeon-hd"></a>

AMD Radeon HD
-------------

<!--
Tickets: #11095 #12482
-->

### Affected graphics cards

<table>
<tr><th>Name</th><th>ID</th><th>Revision number</th></tr>
<tr><td>Advanced Micro Devices, Inc. [AMD/ATI] Mars XTX [Radeon HD 8790M]</td><td>[1002:6606]</td><td></td></tr>
<tr><td>Advanced Micro Devices, Inc. [AMD/ATI] Mars XTX [Radeon HD 8790M]</td><td>[1002:6606]</td><td>(rev ff)</td></tr>
<tr><td>Advanced Micro Devices, Inc. [AMD/ATI] Seymour LP [Radeon HD 6430M]</td><td>[1002:6761]</td><td></td></tr>
<tr><td>Advanced Micro Devices, Inc. [AMD/ATI] Cedar [Radeon HD 5000/6000/7350/8350 Series]</td><td>[1002:68f9]</td><td></td></tr>
<tr><td>Advanced Micro Devices, Inc. [AMD/ATI] Broadway PRO [Mobility Radeon HD 5850]</td><td>[1002:68a1]</td><td></td></tr>
<tr><td>Advanced Micro Devices, Inc. [AMD/ATI] RV730/M96 [Mobility Radeon HD 4650/5165]</td><td>[1002:9480]</td><td></td></tr>
<tr><td>Advanced Micro Devices, Inc. [AMD/ATI] Device [1002:98e4]</td><td>[1002:98e4]</td><td>(rev da)</td></tr>
</table>

### Workaround

For some models, adding `radeon.modeset=0` to the
[[boot options|/doc/first_steps/startup_options/#boot_loader_menu]] fixes the issue.

We need more test results from users: [[!tails_ticket 12482]]

<a id="radeon-r9"></a>

AMD Radeon R9
-------------

<!--
Tickets: #12218 #11850
-->

### Affected graphics cards

<table>
<tr><th>Name</th><th>ID</th><th>Revision number</th></tr>
<tr><td>Advanced Micro Devices, Inc. [AMD/ATI] Hawaii PRO [Radeon R9 290/390]</td><td>[1002:67b1]</td><td></td></tr>
</table>

### Workaround

Adding `radeon.dpm=0` to the
[[boot options|/doc/first_steps/startup_options#boot_loader_menu]]
fixes the issue.

<a id="intel"></a>

Intel
-----

<!--
Ticket: #12219
Ticket: #16224
-->

### Affected graphics cards

Various Intel graphics card.

<!--

<table>
<tr><th>Name</th><th>ID</th><th>Revision number</th></tr>
<tr><td>Intel Corporation</td><td></td><td></td></tr>
</table>

-->

### Workaround

Try adding to the [[boot
options|/doc/first_steps/startup_options#boot_loader_menu]],
one after the other:

* `xorg-driver=intel`
* `nomodeset`
* `nomodeset xorg-driver=vesa`
* `xorg-driver=modesetting`

Otherwise, try starting in the *Troubleshooting Mode*.

<a id="intel-855GM"></a>

Intel 855GM
-----------

<!--
Ticket: #11096, Debian #776911

-->

### Affected graphics cards

<table>
<tr><th>Name</th><th>ID</th><th>Revision number</th></tr>
<tr><td>Intel Corporation 82852/855GM Integrated Graphics Device</td><td>[8086:3582]</td><td>(rev 02)</td></tr>
</table>

<a id="nvidia-tesla"></a>

Nvidia NV50 family (Tesla)
--------------------------

<!--
Ticket: #15491
-->

### Affected graphics cards

<table>
<tr><th>Name</th><th>ID</th><th>Revision number</th></tr>
<tr><td>NVIDIA Corporation MCP89 (GeForce 320M)</td><td>[10de:08a0]</td><td>a2</td></tr>
</table>

### Workaround

Try adding `nomodeset` to the
[[boot options|doc/first_steps/startup_options#boot_loader_menu]].

### Other possibly affected graphics cards

Other graphics cards in the [NV50 family (Tesla)](https://nouveau.freedesktop.org/wiki/CodeNames/#nv50familytesla) might be affected:

<!--
Update this table from time to time as the list of Nvidia cards might change.
Last updated: 2019-07-30
-->

<table>
        <tr>
            <th><strong>Code name</strong>  </th>
            <th><strong>Official Name</strong> </th>
        </tr>
        <tr>
            <td>NV50 (G80)  </td>
            <td>GeForce 8800 (GTS, GTX, Ultra)<br/>Quadro FX (4600 (SDI), 5600) </td>
        </tr>
        <tr>
            <td>NV84 (G84)  </td>
            <td>GeForce 8600 (GT, GTS, M GT, M GS), 8700M GT, GeForce 9500M GS, 9650M GS  <br/>Quadro FX (370, 570, 570M, 1600M, 1700), NVS 320M </td>
        </tr>
        <tr>
            <td>NV86 (G86)  </td>
            <td>GeForce 8300 GS, 8400 (GS, M G, M GS, M GT), 8500 GT, GeForce 9300M G  <br/>Quadro FX 360M, NVS (130M, 135M, 140M, 290) </td>
        </tr>
        <tr>
            <td>NV92 (G92)  </td>
            <td>GeForce 8800 (GT, GS, GTS 512, M GTS, M GTX)  <br/> GeForce 9600 GSO, 9800 (GT, GTX, GTX+, GX2, M GT, M GTX)  <br/> GeForce GTS 150(M), GTS 160M, GTS 240, GTS 250, GTX (260M, 280M, 285M), GT (330, 340)  <br/> Quadro FX (2800M, 3600M, 3700, 3700M, 3800M, 4700 X2), VX 200 </td>
        </tr>
        <tr>
            <td>NV94 (G94)  </td>
            <td>GeForce 9600 (GSO 512, GT, S), 9700M GTS, 9800M GTS, GeForce G 110M, GT 130(M), GT 140  <br/>Quadro FX (1800, 2700M) </td>
        </tr>
        <tr>
            <td>NV96 (G96)  </td>
            <td>GeForce 9400 GT, 9500 (GT, M G), 9600 (M GS, M GT), 9650M GT, 9700M GT  <br/> GeForce G 102M, GT 120  <br/> Quadro FX (380, 580, 770M, 1700M) </td>
        </tr>
        <tr>
            <td>NV98 (G98)  </td>
            <td>GeForce 8400 GS, GeForce 9200M GS, 9300 (GE, GS, M GS)<br/> GeForce G 100, G 105M <br/>Quadro FX (370 LP, 370M), NVS (150M, 160M, 295, 420, 450) </td>
        </tr>
        <tr>
            <td>NVA0 (GT200)  </td>
            <td>GeForce GTX (260, 275, 280, 285, 295)  <br/>Quadro CX, FX (3800, 4800, 5800) </td>
        </tr>
        <tr>
            <td>NVA3 (GT215)  </td>
            <td>GeForce GT (240, 320, 335M), GTS (250M, 260M, 350M, 360M) <br/>Quadro FX 1800M </td>
        </tr>
        <tr>
            <td>NVA5 (GT216)  </td>
            <td>GeForce GT (220, 230M, 240M, 325M, 330M), 315  <br/>Quadro 400, FX 880M, NVS 5100M </td>
        </tr>
        <tr>
            <td>NVA8 (GT218)  </td>
            <td>GeForce 8400 GS, ION 2, GeForce 205, 210, G 210M, 305M, 310(M), 405  <br/>Quadro FX (380 LP, 380M), NVS (300, 2100M, 3100M) </td>
        </tr>
        <tr>
            <td>NVAA (MCP77/MCP78)  </td>
            <td>GeForce 8100, 8200, 8300 mGPU / nForce 700a series, 8200M G </td>
        </tr>
        <tr>
            <td>NVAC (MCP79/MCP7A)  </td>
            <td>ION, GeForce 9300, 9400 mGPU / nForce 700i series, 8200M G, 9100M, 9400M (G) </td>
        </tr>
</table>

<a id="nvidia-maxwell"></a>

Nvidia NV110 family (Maxwell)
-----------------------------

<!--
Ticket: #15116
-->

### Affected graphics cards

<table>
<tr><th>Name</th><th>ID</th><th>Revision number</th></tr>
<tr><td>NVIDIA Corporation GM107 [GeForce GTX 750 Ti]</td><td>?</td><td>a2</td></tr>
<tr><td>NVIDIA Corporation GM204M [GeForce GTX 970M]</td><td>[10de:13d8]</td><td></td></tr>
<tr><td>NVIDIA Corporation GM204M [GeForce GTX 970M]</td><td>[10de:1618]</td><td></td></tr>
</table>

### Workaround

This problem has been fixed for some of these graphic cards.

Otherwise, try adding `nouveau.noaccel=1` or `nouveau.modeset=0` to the
[[boot options|doc/first_steps/startup_options#boot_loader_menu]].

We need more test results from users: [[!tails_ticket 15116]]

### Other possibly affected graphics cards

Other graphics cards in the [NV110 family (Maxwell)](https://nouveau.freedesktop.org/wiki/CodeNames/#nv110familymaxwell) might be affected:

<!--
Update this table from time to time as the list of Nvidia cards might change.
Last updated: 2019-07-30
-->

<table>
        <tr>
            <th><strong>Code name</strong>  </th>
            <th><strong>Official Name</strong> </th>
        </tr>
        <tr>
            <td>NV117 (GM107)  </td>
            <td>GeForce GTX (745, 750, 840M, 845M, 850M, 860M, 950M, 960M) <br/>Quadro K620, K1200, K2200, M1000M, M1200M; GRID M30, M40</td>
        </tr>
        <tr>
            <td>NV118 (GM108)  </td>
            <td>GeForce 830M, 840M, 930M, 940M[X]</td>
        </tr>
        <tr>
            <td>NV120 (GM200)  </td>
            <td>GeForce GTX Titan X</td>
        </tr>
        <tr>
            <td>NV124 (GM204)  </td>
            <td>GeForce GTX (980)</td>
        </tr>
        <tr>
            <td>NV126 (GM206)  </td>
            <td>GeForce GTX (950, 960)</td>
        </tr>
        <tr>
            <td>NV12B (GM20B)  </td>
            <td>Tegra X1</td>
        </tr>
</table>

<a id="nvidia-pascal"></a>

Nvidia NV130 family (Pascal)
----------------------------

<!--
Ticket: #15116
-->

<!--
### Affected graphics cards

<table>
<tr><th>Name</th><th>ID</th><th>Revision number</th></tr>
<tr><td>$VENDOR $DEVICE</td><td>[$VENDOR_ID:$PRODUCT_ID]</td><td>(rev $REVISION_NUMBER)</td></tr>
</table>
-->

### Possibly affected graphics cards

Graphics cards in the [NV130 family (Pascal)](https://nouveau.freedesktop.org/wiki/CodeNames/#nv130familypascal) might be affected:

<!--
Update this table from time to time as the list of Nvidia cards might change.
Last updated: 2019-07-30
-->

<table>
        <tr>
            <th><strong>Code name</strong>  </th>
            <th><strong>Official Name</strong> </th>
        </tr>
        <tr>
            <td>NV132 (GP102)  </td>
            <td>NVIDIA Titan (X, Xp), GeForce GTX 1080 Ti</td>
        </tr>
        <tr>
            <td>NV134 (GP104)  </td>
            <td>GeForce GTX (1070, 1080)</td>
        </tr>
        <tr>
            <td>NV136 (GP106)  </td>
            <td>GeForce GTX 1060</td>
        </tr>
        <tr>
            <td>NV137 (GP107)  </td>
            <td>GeForce GTX (1050, 1050 Ti)</td>
        </tr>
        <tr>
            <td>NV138 (GP108)  </td>
            <td>GeForce GT 1030</td>
        </tr>
</table>

### Workaround

This problem has been fixed for some of these graphic cards.

Otherwise, try adding `nouveau.noaccel=1` or `nouveau.modeset=0` to the
[[boot options|doc/first_steps/startup_options#boot_loader_menu]].

We need more test results from users: [[!tails_ticket 15116]]

Other issues
============

Black screen with switchable graphics computers
-----------------------------------------------

Some computers with switchable graphics (such as Optimus) fail to
choose a video card and end up on a black screen. This has been
reported for MacBook Pro 6,2, MacBook Pro 10,1 Retina, MacBook Pro
15-inch (early 2011) and might affect many others.

There are several possible workarounds for this issue:

* Explicitly select one of the two graphics adapters in the BIOS
  instead of letting the system choose one automatically. If this does
  not solve the problem, try selecting the other graphics adapter.

* For the Mac computers, it is possible to use a third-party
  application, <http://gfx.io/>, to force integrated graphics only through macOS.
  Then restart in that special mode that works with Tails.

* Expert Linux users can also do the following:

  1. Add the `i915.modeset=0 rootpw=pass` option in the
     [[Boot Loader Menu|doc/first_steps/startup_options#boot_loader_menu]].

  2. Create a file `/etc/X11/xorg.conf.d/switchable.conf` with the
     following content:

         Section "Device"
             Identifier "Device0"
             Driver "nouveau"
             BusID "1:0:0"
         EndSection

  4. Restart X with the command:

         service gdm3 restart

  5. After the GNOME session has started, change again the root password with
     the command:

         sudo passwd

For more details, see our ticket on [[!tails_ticket 7505 desc="Video is broken with switchable graphics"]].

<a id=sg-segfault></a>

Cannot start GNOME session with switchable graphics computers
-------------------------------------------------------------

On some computers with switchable graphics, Tails 2.10 and later fails
to start the GNOME session and keeps returning to [[Tails
Greeter|doc/first_steps/startup_options#greeter]].

Starting in *Troubleshooting Mode* works, as well as adding the
`modprobe.blacklist=nouveau` to the [[boot
options|doc/first_steps/startup_options#boot_loader_menu]].

<a id="intel-gm965"></a>

Intel GM965/GL960
-----------------

<!--
Ticket: #12217, Linux #187001
-->

### Affected graphics cards

<table>
<tr><th>Name</th><th>ID</th><th>Revision number</th></tr>
<tr><td>Intel Corporation Mobile GM965/GL960 Integrated Graphics Controller (primary)</td><td>[8086:2a02]</td><td></td></tr>
<tr><td>Intel Corporation Mobile GM965/GL960 Integrated Graphics Controller (secondar)</td><td>[8086:2a03]</td><td></td></tr>
</table>

The laptop crashes while running Tails.

### Workaround

Adding `video=SVIDEO-1:d` to the
[[boot options|/doc/first_steps/startup_options/#boot_loader_menu]] fixes the issue.

<a id="qemu"></a>

Virtual machines with *virt-manager*, *libvirt* and *QEMU*
----------------------------------------------------------

See the
[[dedicated troubleshooting documentation|doc/advanced_topics/virtualization/virt-manager#graphics-issues]]
about graphics issues in Tails running inside a virtual machine
with *virt-manager*.

<a id="vmware"></a>

Virtual machines with *VMware*
------------------------------

To improve support of Tails running inside a virtual machine with
*VMware*, [[install|doc/first_steps/additional_software]] the
`open-vm-tools-desktop` software package in Tails.
