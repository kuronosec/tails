# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: 2019-09-29 19:58+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: Tails translators\n"
"Language-Team: Tails translators <tails-l10n@boum.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Content of: <div>
msgid "[[!meta title=\"Warning! This browser is not anonymous!\"]]"
msgstr ""

#. type: Content of: outside any tag (error?)
msgid "[[!pagetemplate template=\"unsafe_browser_warning.tmpl\"]]"
msgstr ""

#. type: Content of: <p>
msgid "The <em>Unsafe Browser</em> is not using Tor and is not anonymous."
msgstr ""

#. type: Content of: <p>
msgid ""
"<strong>Only use the <em>Unsafe Browser</em> to log in to captive portals.</"
"strong>"
msgstr ""

#. type: Content of: <p><p><p>
msgid "For a safe and anonymous browser, use <em>Tor Browser</em> instead."
msgstr ""

#. type: Content of: <p><p><h2>
msgid "Logging in to captive portals"
msgstr ""

#. type: Content of: <p><p>
msgid ""
"[[!inline pages=\"doc/anonymous_internet/unsafe_browser/captive_portal.inline"
"\" raw=\"yes\" sort=\"age\"]]"
msgstr ""

#. type: Content of: <p><p><p>
msgid "To log in to a captive portal:"
msgstr ""

#. type: Content of: <p><p><ol><li>
msgid "Try visiting any website using the <em>Unsafe Browser</em>."
msgstr ""

#. type: Content of: <p><p><ol><li><p>
msgid ""
"Choose a website that is common in your location, for example a search "
"engine or news site."
msgstr ""

#. type: Content of: <p><p><ol><li>
msgid "You should be redirected to the captive portal instead of the website."
msgstr ""

#. type: Content of: <p><p><ol><li>
msgid "After you logged in to the captive portal, Tor should start."
msgstr ""

#. type: Content of: <p><p><ol><li>
msgid "After Tor is ready, close the <em>Unsafe Browser</em>."
msgstr ""

#. type: Content of: <p><p><ol><li><p>
msgid "You can use <em>Tor Browser</em> and any other application as usual."
msgstr ""
