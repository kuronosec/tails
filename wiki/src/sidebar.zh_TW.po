# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
msgid ""
msgstr ""
"Project-Id-Version: Tails l10n\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2019-09-27 22:08+0000\n"
"PO-Revision-Date: 2018-11-02 17:24+0000\n"
"Last-Translator: Weblate Admin <admin@example.com>\n"
"Language-Team: Tails Chinese translators <jxt@twngo.xyz>\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 2.19.1\n"

#. type: Plain text
#, no-wrap
msgid ""
"<div class=\"download button\">\n"
"  [[<span class=\"install\">Install</span>\n"
"    <span class=\"tails\">Tails [[!inline pages=\"inc/stable_amd64_version\" raw=\"yes\" sort=\"age\"]]</span>\n"
"    <span class=\"date\">[[!inline pages=\"inc/stable_amd64_date\" raw=\"yes\" sort=\"age\"]]</span>|install]]\n"
"</div>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<div id=\"tor_check\" class=\"button\">\n"
"<a href=\"https://check.torproject.org/\">\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!img \"lib/onion.png\" link=\"no\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<!-- Note for translators: You can use <span class=\"twolines\"> if your\n"
"translation of the label below is long and gets split into two lines. -->\n"
"<span>Tor check</span>\n"
"</a>\n"
"</div>\n"
msgstr ""

#. type: Plain text
#, fuzzy, no-wrap
msgid ""
"<div class=\"links\">\n"
"  <ul>\n"
"    <li>[[About|about]]</li>\n"
"    <li>[[Getting started…|getting started]]</li>\n"
"    <li>[[Documentation|doc]]</li>\n"
"    <li>[[Help &amp; Support|support]]</li>\n"
"    <li>[[Contribute]]</li>\n"
"    <li>[[News|news]]</li>\n"
"    <li class=\"new\">[[Jobs|jobs]]</li>\n"
"  </ul>\n"
"</div>\n"
msgstr ""
"<div class=\"links\">\n"
"  <ul>\n"
"    <li>[[關於 Tails|about]]</li>\n"
"    <li>[[準備開始…|getting started]]</li>\n"
"    <li>[[文件|doc]]</li>\n"
"    <li>[[求助 &amp; 支援|support]]</li>\n"
"    <li>[[貢獻參與]]</li>\n"
"    <li>[[最新消息|news]]</li>\n"
"  </ul>\n"
"</div>\n"

#. type: Plain text
#, no-wrap
msgid ""
"<div class=\"donate button\">\n"
"    <a href=\"https://tails.boum.org/donate/?r=sidebar\">Donate</a>\n"
"</div>\n"
msgstr ""
