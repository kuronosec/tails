# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2019-10-07 16:01+0000\n"
"PO-Revision-Date: 2019-05-24 09:56+0200\n"
"Last-Translator: emmapeel <emma.peel@riseup.net>\n"
"Language-Team: Spanish <http://translate.tails.boum.org/projects/tails/home/"
"es/>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 2.20\n"

#. type: Content of: <div>
msgid "[[!meta title=\"Dear Tails user,\"]]"
msgstr ""

#. type: Content of: outside any tag (error?)
#, fuzzy
#| msgid ""
#| "[[!meta title=\"Welcome to Tails!\"]] [[!meta stylesheet=\"home\" rel="
#| "\"stylesheet\" title=\"\"]] [[!meta robots=\"noindex\"]] [[!meta script="
#| "\"home\"]]"
msgid ""
"[[!meta stylesheet=\"home\" rel=\"stylesheet\" title=\"\"]] [[!meta robots="
"\"noindex\"]] [[!meta script=\"home\"]]"
msgstr ""
"[[!meta title=\"¡Bienvenido/a a Tails!\"]] [[!meta stylesheet=\"home\" rel="
"\"stylesheet\" title=\"\"]] [[!meta robots=\"noindex\"]] [[!meta script="
"\"home\"]]"

#. type: Content of: <div><p>
#, fuzzy
#| msgid ""
#| "Today, we are asking you to help Tails. You are able to download Tails "
#| "for free because we believe that nobody should have to pay to be safe "
#| "while using computers. And we want to keep it this way. <span class="
#| "\"highlight\">If everyone reading this donated $6, our fundraiser would "
#| "be done in one day.</span> The price of a USB stick is all we need."
msgid ""
"Today, we are asking you to help Tails. You can enjoy Tails for free because "
"we believe that nobody should have to pay to be safe while using computers. "
"And we want to keep it this way. <span class=\"highlight\">If everyone "
"reading this donated $6, our fundraiser would be done in one day.</span> The "
"price of a USB stick is all we need."
msgstr ""
"Hoy te pedimos que ayudes a Tails. Descargaste Tails gratuitamente porque "
"creemos que nadie debería pagar para estar seguro mientras usa una "
"computadora. Y queremos mantenerlo así. <span class=\"highlight\">Si cada "
"persona que lee esto donase 5€, nuestra campaña de donación se acabaría en "
"un día.</span> Todo lo que necesitamos es el precio de una memoria USB."

#. type: Content of: <div><p>
msgid ""
"But, not everyone can donate. When you donate, you are offering to many "
"others who need it, this precious tool that is Tails."
msgstr ""
"Pero no todos pueden donar. Cuando lo haces, estás haciendo llegar la "
"preciosa herramienta que es Tails a muchas otras personas que lo necesitan."

#. type: Content of: <div><div>
#, fuzzy
#| msgid "<a href=\"https://tails.boum.org/donate/?r=home\">Donate</a>"
msgid "<a href=\"https://tails.boum.org/donate?r=home\">Donate</a>"
msgstr "<a href=\"https://tails.boum.org/donate/?r=home\">Donar</a>"

#. type: Content of: <div>
msgid "[[!inline pages=\"news\" raw=\"yes\" sort=\"age\"]]"
msgstr "[[!inline pages=\"news.es\" raw=\"yes\" sort=\"age\"]]"

#~ msgid "[[!inline pages=\"home/donate\" raw=\"yes\" sort=\"age\"]]"
#~ msgstr "[[!inline pages=\"home/donate.es\" raw=\"yes\" sort=\"age\"]]"

#~ msgid "[[!inline pages=\"home/tor_check\" raw=\"yes\" sort=\"age\"]]"
#~ msgstr "[[!inline pages=\"home/tor_check.es\" raw=\"yes\" sort=\"age\"]]"

#~ msgid ""
#~ "<a href=\"https://tails.boum.org/install/check/\"> [[!img \"lib/onion.png"
#~ "\" link=\"no\"]] <span>Tor check</span> </a>"
#~ msgstr ""
#~ "<a href=\"https://tails.boum.org/install/check/\">[[!img \"lib/onion.png"
#~ "\" link=\"no\"]]\n"
#~ "<span class=\"twolines\">Comprobación de Tor</span></a>"

#~ msgid ""
#~ "<a id=\"donate\" class=\"random-message\" href=\"https://tails.boum.org/"
#~ "donate?r=h\" data-display-probability=\"0.1\">"
#~ msgstr ""
#~ "<a id=\"donate\" class=\"random-message\" href=\"https://tails.boum.org/"
#~ "donate?r=h\" data-display-probability=\"0.1\">"

#~ msgid "Tails needs donations from users like you."
#~ msgstr "Tails necesita donaciones de usuarios como tu."

#~ msgid "Donate"
#~ msgstr "Donar"

#~ msgid "</a> [[!inline pages=\"news\" raw=\"yes\" sort=\"age\"]]"
#~ msgstr "</a> [[!inline pages=\"news.es\" raw=\"yes\" sort=\"age\"]]"
